### NEW RELEASE FOR PACKAGE `kernel-xanmod-edge`+`kernel-xanmod-exptl`

Changes for 5.14.0-xm1.0 + 5.14.0-xm1.0e20210902:
(※ Refer commit tags to https://github.com/xanmod/linux):
* 8c839bfa06e5 Linux 5.14.0-xanmod1
* 6317b65b9a95 netfilter: Add full cone NAT support
* 74a207081257 fs/ntfs3: Restyle comments to better align with kernel-doc
* cdba19cfda59 fs/ntfs3: Rework file operations
* 46a09b9ebba0 fs/ntfs3: Remove fat ioctl's from ntfs3 driver for now
* a1508640c471 fs/ntfs3: Restyle comments to better align with kernel-doc
* 82380696c7c6 fs/ntfs3: Fix error handling in indx_insert_into_root()
* ecda7a75d9b7 fs/ntfs3: Potential NULL dereference in hdr_find_split()
* df5d9bb6c1bc fs/ntfs3: Fix error code in indx_add_allocate()
* 548f3a8b3454 fs/ntfs3: fix an error code in ntfs_get_acl_ex()
* a84a0bb4c9aa fs/ntfs3: add checks for allocation failure
* 582437a43169 fs/ntfs3: Use kcalloc/kmalloc_array over kzalloc/kmalloc
* 3d45b1852f34 fs/ntfs3: Do not use driver own alloc wrappers
* 7b2edf8084fb fs/ntfs3: Use kernel ALIGN macros over driver specific
* 6071926968f1 fs/ntfs3: Restyle comment block in ni_parse_reparse()
* 75d85f88e9c3 fs/ntfs3: Remove unused including <linux/version.h###
* 208e2b5f62f9 fs/ntfs3: Fix fall-through warnings for Clang
* dba411671cc7 fs/ntfs3: Fix one none utf8 char in source file
* 448da00d3a07 fs/ntfs3: Remove unused variable cnt in ntfs_security_init()
* 7848a6f50e1c fs/ntfs3: Fix integer overflow in multiplication
* 9457b121f871 fs/ntfs3: Add ifndef + define to all header files
* cfab079d7add fs/ntfs3: Use linux/log2 is_power_of_2 function
* 6ac3cde638e1 fs/ntfs3: Fix various spelling mistakes
* 7aeab99b2591 fs/ntfs3: Add MAINTAINERS
* 9e3c3d099f18 fs/ntfs3: Add NTFS3 in fs/Kconfig and fs/Makefile
* 93ead8fb5403 fs/ntfs3: Add Kconfig, Makefile and doc
* fd63d37c1b9e fs/ntfs3: Add NTFS journal
* d3f5f3ad9f8c fs/ntfs3: Add compression
* cb25eb8b77e4 fs/ntfs3: Add attrib operations
* 9d01713d336b fs/ntfs3: Add file operations and implementation
* 018d7923d769 fs/ntfs3: Add bitmap
* 5bf9cd7aca35 fs/ntfs3: Add initialization of super block
* 7f3f48560484 fs/ntfs3: Add headers and misc files
* 418f552b08fa pci: Enable overrides for missing ACS capabilities
* 080344179972 x86/kconfig: more uarches for kernel 5.8+
* af2d931bdfa5 init: wait for partition and retry scan
* a41a87f11c0c drivers: initialize ata before graphics
* 80e01b0e579c locking: rwsem: spin faster
* 5948ca1226e4 firmware: Enable stateless firmware loading
* 09e053e2b26e intel_rapl: Silence rapl trace debug
* 0196fd65dbcd SAUCE: binder: give binder_alloc its own debug mask file
* 047096213504 SAUCE: binder: turn into module
* 7183da046404 SAUCE: ashmem: turn into module
* 97e857f92dde sysctl: add sysctl to disallow unprivileged CLONE_NEWUSER by default
* f6002a622ece net-tcp_bbr: v2: Fix missing ECT markings on retransmits for BBRv2
* 68856d40909b net-tcp_bbr: v2: don't assume prior_cwnd was set entering CA_Loss
* 7de3f36f619c net-tcp_bbr: v2: remove cycle_rand parameter that is unused in BBRv2
* dfedade9dca8 net-tcp_bbr: v2: remove field bw_rtts that is unused in BBRv2
* 7882fddcbfb3 net-tcp_bbr: v2: remove unnecessary rs.delivered_ce logic upon loss
* 729237398032 net-tcp_bbr: v2: BBRv2 ("bbr2") congestion control for Linux TCP
* 002c56a891c6 net-tcp: add fast_ack_mode=1: skip rwin check in tcp_fast_ack_mode__tcp_ack_snd_check()
* aaac92aea9ff net-tcp: re-generalize TSO sizing in TCP CC module API
* 7df691bf6754 net-tcp: add new ca opts flag TCP_CONG_WANTS_CE_EVENTS
* 358fdaea39ea net-tcp_bbr: v2: set tx.in_flight for skbs in repair write queue
* 88004ea5d93d net-tcp_bbr: v2: adjust skb tx.in_flight upon split in tcp_fragment()
* 817fad8477b2 net-tcp_bbr: v2: adjust skb tx.in_flight upon merge in tcp_shifted_skb()
* 3fe3672f973c net-tcp_bbr: v2: factor out tx.in_flight setting into tcp_set_tx_in_flight()
* 1a21903c691c net-tcp_bbr: v2: introduce ca_ops-###skb_marked_lost() CC module callback API
* a88fb3d7e3cb net-tcp_bbr: v2: export FLAG_ECE in rate_sample.is_ece
* 3c836d6a3784 net-tcp_bbr: v2: count packets lost over TCP rate sampling interval
* a9dca3ca500c net-tcp_bbr: v2: snapshot packets in flight at transmit time and pass in rate_sample
* 72b3d52fbcfa net-tcp_bbr: v2: shrink delivered_mstamp, first_tx_mstamp to u32 to free up 8 bytes
* 4beae98fdebe net-tcp_rate: account for CE marks in rate sample
* e1fa0b1b4d71 net-tcp_rate: consolidate inflight tracking approaches in TCP
* e99a898dd29e net-tcp_bbr: broaden app-limited rate sample detection
* 50bc1ffc2bee lib: zstd: Upgrade to latest upstream zstd version 1.4.10
* 2f701516ba0f lib: zstd: Add decompress_sources.h for decompress_unzstd
* 041ebfb541fc lib: zstd: Add kernel-specific API
* 965d7f45c671 futex: Implement mechanism to wait on any of several futexes
* 6e77d352e9db futex2: proton
* b1a704d844e6 futex2: Add sysfs entry for syscall numbers
* fd4d81ccbed0 perf bench: Add futex2 benchmark tests
* 8d639eab4d4f selftests: futex2: Add waitv test
* 81056be7e148 selftests: futex2: Add wouldblock test
* 33eaaee8dbec selftests: futex2: Add timeout test
* ed8b67078f18 selftests: futex2: Add wake/wait test
* c98b09c1fbe6 docs: locking: futex2: Add documentation
* a1fa8fc751af futex2: Implement vectorized wait
* 60e07a8eb7ac futex2: Implement wait and wake functions
* b0a3be52c5df mm/vmscan: add sysctl knobs for protecting the working set
* fd0956e4d4f8 mm: multigenerational lru: documentation
* 5b37aedba708 mm: multigenerational lru: Kconfig
* 29c00f025ced mm: multigenerational lru: user interface
* 1d0e6093d725 mm: multigenerational lru: eviction
* b87b0bd8b0e8 mm: multigenerational lru: aging
* 6658557afe49 mm: multigenerational lru: mm_struct list
* 047aa506412f mm: multigenerational lru: protection
* 6016b640b11e mm: multigenerational lru: groundwork
* e224026584fc mm/vmscan.c: refactor shrink_node()
* 350e3508f8ef mm: x86: add CONFIG_ARCH_HAS_NONLEAF_PMD_YOUNG
* 3bdf11d1feca mm: x86, arm64: add arch_has_hw_pte_young()
* 81632bc71a55 char/lrng: add power-on and runtime self-tests
* d3c8b63af812 char/lrng: add interface for gathering of raw entropy
* c635a8aeeca0 char/lrng: add SP800-90B compliant health tests
* 31720489c680 char/lrng: add Jitter RNG fast noise source
* 413a2475361c crypto: provide access to a static Jitter RNG state
* b73b9363c446 char/lrng: add kernel crypto API PRNG extension
* 3c8d95825b3e char/lrng: add SP800-90A DRBG extension
* 772b4355074b crypto: drbg - externalize DRBG functions for LRNG
* 289ae6f42596 char/lrng: add common generic hash support
* 255b2f8211e7 char/lrng: add switchable DRNG support
* 44dab88512d3 char/lrng: sysctls and /proc interface
* 70d7ec6c96f9 char/lrng: allocate one DRNG instance per NUMA node
* 80dde18aac33 drivers: Introduce the Linux Random Number Generator
* 072a55ef17e9 clockevents, hrtimer: Make hrtimer granularity and minimum hrtimeout configurable in sysctl. Set default granularity to 100us and min timeout to 500us
* 4c9484b388e6 time: Don't use hrtimer overlay when pm_freezing since some drivers still don't correctly use freezable timeouts.
* 33c780391af7 hrtimer: Replace all calls to schedule_timeout_uninterruptible of potentially under 50ms to use schedule_msec_hrtimeout_uninterruptible
* 1db9c631c9ee hrtimer: Replace all calls to schedule_timeout_interruptible of potentially under 50ms to use schedule_msec_hrtimeout_interruptible.
* 9de56a69843a hrtimer: Replace all schedule timeout(1) with schedule_min_hrtimeout()
* d12a11b569cd timer: Convert msleep to use hrtimers when active.
* 489c9796e4f4 time: Special case calls of schedule_timeout(1) to use the min hrtimeout of 1ms, working around low Hz resolutions.
* d918ad3d6c2a hrtimer: Create highres timeout variants of schedule_timeout functions.
* 0f184028885b XANMOD: fair: Remove all energy efficiency functions
* fc28a768fbb5 XANMOD: Makefile: Turn off loop vectorization for GCC -O3 optimization level
* ad53ae3e9fc8 XANMOD: init/Kconfig: Enable -O3 KBUILD_CFLAGS optimization for all architectures
* 441d0b82d152 XANMOD: lib/kconfig.debug: disable default CONFIG_SYMBOLIC_ERRNAME and CONFIG_DEBUG_BUGVERBOSE
* 19dc9716bba5 XANMOD: scripts: disable the localversion "+" tag of a git repo
* 933a3df33c87 XANMOD: cpufreq: tunes ondemand and conservative governor for performance
* f76b2038e5fb XANMOD: mm/vmscan: vm_swappiness = 30 decreases the amount of swapping
* 9171c3f9b851 XANMOD: sched/autogroup: Add kernel parameter and config option to enable/disable autogroup feature by default
* 0f67d47b522f XANMOD: dcache: cache_pressure = 50 decreases the rate at which VFS caches are reclaimed
* 7fb8252e6887 XANMOD: kconfig: set PREEMPT and RCU_BOOST without delay by default
* aeadb68c9cc4 XANMOD: kconfig: add 500Hz timer interrupt kernel config option
* 1330aad8774a XANMOD: block: set rq_affinity to force full multithreading I/O requests
* ad0a45a39614 XANMOD: block, bfq: change BLK_DEV_ZONED depends to IOSCHED_BFQ
* 570e1b5b7e35 XANMOD: elevator: set default scheduler to bfq for blk-mq
* 7d2a07b76933 Linux 5.14
* [xanmod-copr] add drivers for winesync, built as modules
* [xanmod-copr:exptl] refresh Makefile patch for 5.14.y
