### NEW RELEASE FOR PACKAGE `kernel-xanmod-edge`+`kernel-xanmod-exptl`

Changes for 5.14.2-xm1.0 + 5.14.2-xm1.0e20210902:
(※ Refer commit tags to https://github.com/xanmod/linux):
* 6bbe0e2cbb68 Linux 5.14.2-xanmod1
* 148cdf7095df Merge tag 'v5.14.2' into 5.14
* bbdd3de144fc Linux 5.14.2
* f05cec9e0808 media: stkwebcam: fix memory leak in stk_camera_probe
* 8ab355d672f9 ALSA: usb-audio: Work around for XRUN with low latency playback
* d789944b05ad ALSA: pcm: fix divide error in snd_pcm_lib_ioctl
* 5f9aadda1d05 ALSA: hda/realtek: Workaround for conflicting SSID on ASUS ROG Strix G17
* 476081ac2afe ALSA: usb-audio: Fix regression on Sony WALKMAN NW-A45 DAC
* b0406dde5e5f ALSA: hda/realtek: Quirk for HP Spectre x360 14 amp setup
* 9260f15e0ad0 HID: usbhid: Fix warning caused by 0-length input reports
* dbf698e1f4ad HID: usbhid: Fix flood of "control queue full" messages
* bb6eb160c0d0 USB: serial: cp210x: fix flow-control error handling
* 363715aee0ef USB: serial: cp210x: fix control-characters error handling
* 8c3084b21f95 USB: serial: pl2303: fix GL type detection
* 1dc13eed10fe xtensa: fix kconfig unmet dependency warning for HAVE_FUTEX_CMPXCHG
* f50c411452ab ext4: fix e2fsprogs checksum failure for mounted filesystem
* f8ea208b3fbb ext4: fix race writing to an inline_data file while its xattrs are changing

---

### NEW RELEASE FOR PACKAGE `kernel-xanmod-cacule`

Changes for 5.14.2-xm1cacule.0:
(※ Refer commit tags to https://github.com/xanmod/linux):
* abdb635c48c5 Linux 5.14.2-xanmod1-cacule
* 6051b8d026f3 Merge tag 'v5.14.2' into 5.14-cacule
* bbdd3de144fc Linux 5.14.2
* f05cec9e0808 media: stkwebcam: fix memory leak in stk_camera_probe
* 8ab355d672f9 ALSA: usb-audio: Work around for XRUN with low latency playback
* d789944b05ad ALSA: pcm: fix divide error in snd_pcm_lib_ioctl
* 5f9aadda1d05 ALSA: hda/realtek: Workaround for conflicting SSID on ASUS ROG Strix G17
* 476081ac2afe ALSA: usb-audio: Fix regression on Sony WALKMAN NW-A45 DAC
* b0406dde5e5f ALSA: hda/realtek: Quirk for HP Spectre x360 14 amp setup
* 9260f15e0ad0 HID: usbhid: Fix warning caused by 0-length input reports
* dbf698e1f4ad HID: usbhid: Fix flood of "control queue full" messages
* bb6eb160c0d0 USB: serial: cp210x: fix flow-control error handling
* 363715aee0ef USB: serial: cp210x: fix control-characters error handling
* 8c3084b21f95 USB: serial: pl2303: fix GL type detection
* 1dc13eed10fe xtensa: fix kconfig unmet dependency warning for HAVE_FUTEX_CMPXCHG
* f50c411452ab ext4: fix e2fsprogs checksum failure for mounted filesystem
* f8ea208b3fbb ext4: fix race writing to an inline_data file while its xattrs are changing

---

### NEW RELEASE FOR PACKAGE `kernel-xanmod-lts`

Changes for 5.10.63-xm1.0:
(※ Refer commit tags to https://github.com/xanmod/linux):
* 34efa8cd4a9b Linux 5.10.63-xanmod1
* 27cb01a6a97b Merge tag 'v5.10.63' into 5.10
* e07f317d5a28 Linux 5.10.63
* 4405ea221dea media: stkwebcam: fix memory leak in stk_camera_probe
* ad5e13f15db7 fuse: fix illegal access to inode with reused nodeid
* 40ba433a85db new helper: inode_wrong_type()
* ded9137fcf0d spi: Switch to signed types for *_native_cs SPI controller fields
* 55bb5193cec5 serial: 8250: 8250_omap: Fix possible array out of bounds access
* 8e41134a92a5 ALSA: pcm: fix divide error in snd_pcm_lib_ioctl
* 4ffde17862b0 ALSA: hda/realtek: Workaround for conflicting SSID on ASUS ROG Strix G17
* 4ee2686b3745 ALSA: hda/realtek: Quirk for HP Spectre x360 14 amp setup
* 2808d59fb29b cryptoloop: add a deprecation warning
* 61a038f80c80 perf/x86/amd/power: Assign pmu.module
* ec9a82e034f6 perf/x86/amd/ibs: Work around erratum #1197
* 23c29490b84d ceph: fix possible null-pointer dereference in ceph_mdsmap_decode()
* d2064a1444d6 perf/x86/intel/pt: Fix mask of num_address_ranges
* 0e74bba60452 qede: Fix memset corruption
* 35f223cb21b1 net: macb: Add a NULL check on desc_ptp
* cf50d02e474b qed: Fix the VF msix vectors flow
* 2177c4943e40 reset: reset-zynqmp: Fixed the argument data type
* 9872349b088d gpu: ipu-v3: Fix i.MX IPU-v3 offset calculations for (semi)planar U/V formats
* b983d60292a6 ARM: OMAP1: ams-delta: remove unused function ams_delta_camera_power
* bc860c3f0945 xtensa: fix kconfig unmet dependency warning for HAVE_FUTEX_CMPXCHG
* b1075d2a7052 static_call: Fix unused variable warn w/o MODULE
* ae16b7c66837 Revert "Add a reference to ucounts for each cred"
* 1aa3f27e592d Revert "cred: add missing return error code when set_cred_ucounts() failed"
* 0c1443874e1c Revert "ucounts: Increase ucounts reference counter before the security hook"
* 0479b2bd2959 ubifs: report correct st_size for encrypted symlinks
* 3ac01789f6d9 f2fs: report correct st_size for encrypted symlinks
* 894a02236d0d ext4: report correct st_size for encrypted symlinks
* b8c298cf57dc fscrypt: add fscrypt_symlink_getattr() for computing st_size
* 09a379549620 ext4: fix race writing to an inline_data file while its xattrs are changing
